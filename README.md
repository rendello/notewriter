
# Usage

For use as a command-line tool. simply run 'note' (once it's installed
in your path or in /usr/local/bin, etc.) in a directory set aside for
a certain subject (eg. ~/home/$USER/Classes/Math), and the program will
create a text file named after the current date and open said file in Vim.
