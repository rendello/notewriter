#!/usr/bin/python3

from datetime import datetime
import os
import time

fileDate = datetime.now().strftime("%Y-%m-%d")
simpleDate = datetime.now().strftime("%A, %b %d, %Y")
currentTime = datetime.now().strftime("%H:%M")

filePath = os.getcwd() + "/" + fileDate

mode = 'a' if os.path.exists(filePath) else 'w'

with open(filePath, mode) as f:
    if mode =='w':
        f.write("[ " + os.path.basename(os.getcwd()) + " ]\n@ " + simpleDate + "\n")
    f.write("\n@ " + currentTime + "\n\n\n")

os.system("vim " + filePath + " -c \"normal G $\"")
